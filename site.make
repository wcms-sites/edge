core = 7.x
api = 2

; uw_conditional_rulesets
projects[uw_conditional_rulesets][type] = "module"
projects[uw_conditional_rulesets][download][type] = "git"
projects[uw_conditional_rulesets][download][url] = "https://git.uwaterloo.ca/wcms/uw_conditional_rulesets.git"
projects[uw_conditional_rulesets][download][tag] = "7.x-2.1"
projects[uw_conditional_rulesets][subdir] = ""

; uw_edge_experiences
projects[uw_edge_experiences][type] = "module"
projects[uw_edge_experiences][download][type] = "git"
projects[uw_edge_experiences][download][url] = "https://git.uwaterloo.ca/wcms/uw_edge_experiences.git"
projects[uw_edge_experiences][download][tag] = "7.x-1.0"
projects[uw_edge_experiences][subdir] = ""
